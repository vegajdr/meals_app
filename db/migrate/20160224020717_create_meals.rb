class CreateMeals < ActiveRecord::Migration
  def change
    create_table :meals do |t|
      t.integer  :user_id
      t.string   :name
      t.float    :price
      t.timestamps null: false
    end
  end
end
